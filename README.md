# Public OMOP Mappings from Genomics England


## These mappings have been moved

Please note that the OMOP mappings have been moved to sit alongside Genomics England's other publications. They can now be found [here.](https://gitlab.com/genomicsengland/genomics_england_publications/public-omop-mappings)

